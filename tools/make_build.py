import sys
import os
import hammer_config as config

usage = """
Usage:
    hammer make build [options...] [build-tool-options...]
            To build an already-generated project with using native build tool.

            Both options and build-tool-options are directly passed to cmake
            and can be one or several allowable cmake options
            excluding '--build <dir>' that is overrided by this build system.

            The most likely options are:
            --target <tgt>...
                    Build <tgt> instead of the default target.
                    Multiple targets may be given, separated by spaces.

            --clean-first
                    Rebuild target(s)
            
Or:
    hammer make build <-h|--help>
            To display this help.
"""

args = sys.argv[1:-1]

if '-h' in args or '--help' in args:
    print(usage)
    exit(0)

if '--build' in args:
    print('Error: You should not directly specify a build directory.', file=sys.stderr)
    print(usage)
    sys.exit(255)

buildcfg = '--config {}'.format(config.buildcfg) if config.buildcfg else ''
buildopt = '-- ' + config.buildopt if config.buildopt else ''

cmake_cmd = 'cmake --build {bindir} {cfg} {options} {buildopt}'.format(bindir=config.outdir, cfg=buildcfg, options=' '.join(args), buildopt=buildopt)

print(cmake_cmd)
errorcode = os.system(cmake_cmd)
sys.exit(errorcode)
