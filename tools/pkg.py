import subscript

commands = {
    'fetch': {
        'description': ['Command "fetch" prepares packages to build.',
                        'It downloads, unpacks and stores sources of all necessary packages',
                        'that the project is dependent on.'],
        'script': 'pkg_fetch.py'
        },
    'build': {
        'description': ['Command "build" builds the project packages.',
                        'It configures and builds all necessary packages',
                        'that the project is dependent on.'],
        'script': 'pkg_build.py'
        },
    'install': {
        'description': ['Command "install" install the project packages.',
                        'It copies build artifacts of all necessary packages',
                        'to there where they are able to be found by cmake.'],
        'script': 'pkg_install.py'
        }
    }

usage = """
Usage:
    hammer pkg command [options...]
            To call one of package manager commands: {list}.
            
            {desc}
Or:
    hammer pkg [-h|--help]
            To display this help.
"""

subscript.run('package manager command', usage, commands)