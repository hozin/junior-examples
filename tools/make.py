import subscript

commands = {
    'config': {
        'description': ['Command "config" prepares the project to build.',
                        'It creates native project files that can to be built by a build tool.'],
        'script': 'make_config.py'
        },
    'build': {
        'description': ['Command "build" builds configured project with native build tool.',
                        'It builds all project targets by default.'],
        'script': 'make_build.py'
        },
    'test': {
        'description': ['Command "test" runs project tests.',
                        'It runs all project tests by default.'],
        'script': 'make_test.py'
        },
    'clean': {
        'description': ['Command "clean" deletes build artifacts.',
                        'It deletes artifacts of all targets by default.'],
        'script': 'make_clean.py'
        }
    }

usage = """
Usage:
    hammer make command [options...]
            To call one of make commands: {list}.
            
            {desc}
Or:
    hammer make [-h|--help]
            To display this help.
"""

subscript.run('make command', usage, commands)