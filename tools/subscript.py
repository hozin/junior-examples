import sys
import os

def run(name, usage, scripts): # no return
    usage = usage.format( list=', '.join(scripts.keys()),
            desc='\n\n            '.join(['\n            '.join(s['description']) for s in scripts.values()]))
    
    if len(sys.argv[1:-1]) == 0 or sys.argv[1] in ['-h', '--help']:
        print(usage)
        sys.exit(255)

    script = sys.argv[1]

    if not script in scripts.keys():
        print("Error: Unknown {} '{}'.".format(name, script), file=sys.stderr)
        print(usage)
        sys.exit(255)

    script = os.path.normpath(os.path.join(sys.path[0], scripts[script]['script']))

    if not os.path.isfile(script):
        print('Error: {} script file "{}" not found.'.format(name.title(), script), file=sys.stderr)
        sys.exit(255)

    errorcode = os.system('python {} {}'.format(script, ' '.join(sys.argv[2:])))
    sys.exit(errorcode)