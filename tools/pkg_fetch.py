import sys
import os
import json
import platform
import hammer_config as config

usage = """
Usage:
    hammer pkg fetch [options...]
            To download, unpack and store sources of the packages.

            Options:
            --force
                erase all artifacts of the package(s) and perform fetch again.
            
            --package <pkg-name>...
                perform the operation for specified package(s) only,
                list of package names are space separated.
            
Or:
    hammer pkg fetch <-h|--help>
            To display this help.
"""

args = sys.argv[1:-1]

if '-h' in args or '--help' in args:
    print(usage)
    exit(0)

if not config.pkgcfg:
    print("Note: packages configuration was not specified, nothing to do.", file=sys.stderr)
    exit(0)

pkgconfigfile = os.path.join(os.path.dirname(config.pkgcfg), '{}-{}'.format( platform.system().lower(), os.path.basename(config.pkgcfg)))
if not os.path.isfile(pkgconfigfile):
    print('Error: packages configuration file "{}" has not been found.'.format(pkgconfigfile), file=sys.stderr)
    exit(255)

if not os.path.isdir(config.pkgdir):
    os.makedirs(config.pkgdir)

conan_cmd = 'conan install {pkgcfg} {opts} -g cmake -if "{pkgdir}" -s build_type={build_type}'\
            .format(pkgcfg=pkgconfigfile, pkgdir=config.pkgdir, build_type=config.buildcfg, opts=' '.join(args))

print(conan_cmd)
errorcode = os.system(conan_cmd)
sys.exit(errorcode)