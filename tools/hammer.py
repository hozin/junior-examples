import subscript

subsystems = {
    'make': {
        'description': ['Subsystem "make" is responsible for confugure and build the project.'],
        'script': 'make.py'
        },
    'pkg': {
        'description': ['Subsystem "packages" is responsible for download, unzip, configure,',
                        'build and install external packages.'],
        'script': 'pkg.py'
        },
    'autotesting': {
        'description': ['Subsystem "autotesting" is responsible for executing automation tests.'],
        'script': 'autotesting.py'
        }
    }

usage = """
Usage:
    hammer subsystem [command] [options...]
            To invoke one of subsystems: {list}.
            
            {desc}
Or:
    hammer [-h|--help]
            To display this help.
"""

subscript.run('subsystem', usage, subsystems)