import sys
import subprocess
import hammer_config as config

usage = """
Usage:
    hammer make test [options...]
            To execute test driver program (CTest).

            Options are directly passed to ctest and can be one or several allowable ctest options.

            The most likely options are:
            --verbose
                    Enable to show all test output.
            
            --rerun-failed
                    Run only the tests that failed previously.
            
            --tests-regex <regex>
                    Run tests matching regular expression.
            
Or:
    hammer make test <-h|--help>
            To display this help.
"""

args = sys.argv[1:-1]

if '-h' in args or '--help' in args:
    print(usage)
    exit(0)

ctest_cmd = ['ctest']

if config.buildcfg:
    ctest_cmd.extend(['--build-config', config.buildcfg])

ctest_cmd.extend(args)

print(' '.join(ctest_cmd))
result = subprocess.run(ctest_cmd, cwd=config.outdir)
exit(result.returncode)