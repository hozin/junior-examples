@echo off

set repo_root=%~d0%~p0

python %repo_root%tools\hammer.py %* --hammer_config=%repo_root%hammer.cfg.user

exit /B %errorlevel%